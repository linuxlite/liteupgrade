Lite Upgrade 
=================

Software to upgrade Linux Lite from within a Series.

![](https://imgur.com/rNWSd7M.png)

![](https://imgur.com/AatIadz.png)

![](https://imgur.com/rdizO54.png)

![](https://imgur.com/3DEeOKA.png)

![](https://imgur.com/rir0YY4.png)

## License ![License](https://img.shields.io/badge/license-GPLv2-green.svg)

This project is under the GPLv2 license. Unless otherwise stated in individual files.

## Authors
- [Jerry Bezencon](https://github.com/linuxlite/)
- [Ralphy](https://github.com/ralphys)
- [Misko-2083](https://github.com/Misko-2083/)
