#!/bin/bash
#----------------------------------------------------------------
# Description: Linux Lite Upgrade Script - System
# About: For Linux Lite 7.- Series only
# Licence: GPLv2 Sharing is Caring
# Author: Jerry Bezencon, Ralphy
# Credits: Misko_2083
# Website: https://www.linuxliteos.com
#----------------------------------------------------------------

# Ensure multi-language support
export LANG=C

# Kill off any package managers that may be running
if [ "$(pidof synaptic)" ]; then killall -9 synaptic; else :; fi
if [ -z "$(pgrep gdebi-gtk)" ]; then :; else killall -9 gdebi-gtk; fi

# Keep it clean to reduce errors
apt-get clean all 2>&1 | tee -a $LOGFILE

LL70SYSTEM() {
# Invoke arrays
ARRAYB=()
ARRAYC=()

#Check the arch
arch=$(uname -m)

case "$arch" in
    x86)    arch="x86"  ;;
    i?86)   arch="x86"  ;;
    amd64)  arch="amd64"    ;;
    x86_64) arch="amd64"    ;;
    * ) echo "Your Arch '$arch' -> Its not supported."  ;;
esac

# Populate arrays based on the arch
case "$arch" in
    x86)
    ARRAYB+=('BUPETCSKEL7064')
    ARRAYC+=('UPDPKGS')
    ARRAYC+=('ERASEDPKGS')
    ARRAYC+=('INPKCL')
    ARRAYC+=('UPGLOFFICE')
    ARRAYC+=('OUTLOPKGS')
    ARRAYC+=('UPLSBISSUE')
    ARRAYC+=('UPDGRUB')
    ARRAYC+=('UPDPLYM')
    ;;
    amd64)
    ARRAYB+=('BUPETCSKEL7064')
    ARRAYC+=('UPDPKGS')
    ARRAYC+=('ERASEDPKGS')
    ARRAYC+=('INPKCL64')
    ARRAYC+=('UPGLOFFICE')
    ARRAYC+=('OUTLOPKGS')
    ARRAYC+=('UPLSBISSUE')
    ARRAYC+=('UPDGRUB')
    ARRAYC+=('UPDPLYM')
    ;;
esac
}

LL72SYSTEM() {
# Invoke arrays
ARRAYB=()
ARRAYC=()

#Check the arch
arch=$(uname -m)

case "$arch" in
    x86)    arch="x86"  ;;
    i?86)   arch="x86"  ;;
    amd64)  arch="amd64"    ;;
    x86_64) arch="amd64"    ;;
    * ) echo "Your Arch '$arch' -> Its not supported."  ;;
esac

# Populate arrays based on the arch
case "$arch" in
    x86)
    ARRAYB+=('BUPETCSKEL7264')
    ARRAYC+=('UPDPKGS')
    ARRAYC+=('ERASEDPKGS')
    ARRAYC+=('INPKCL')
    ARRAYC+=('UPGLOFFICE')
    ARRAYC+=('OUTLOPKGS')
    ARRAYC+=('UPLSBISSUE')
    ARRAYC+=('UPDGRUB')
    ARRAYC+=('UPDPLYM')
    ;;
    amd64)
    ARRAYB+=('BUPETCSKEL7264')
    ARRAYC+=('UPDPKGS')
    ARRAYC+=('ERASEDPKGS')
    ARRAYC+=('INPKCL64')
    ARRAYC+=('UPGLOFFICE')
    ARRAYC+=('OUTLOPKGS')
    ARRAYC+=('UPLSBISSUE')
    ARRAYC+=('UPDGRUB')
    ARRAYC+=('UPDPLYM')
    ;;
esac
}

# Backup /etc/skel/ Linux Lite 7.0

BUPETCSKEL7064() {
  log_upgrade "BUPETCSKEL7064"
  echo "#⚫ Backing up /etc/skel files..." && sleep 2
  mkdir -p /etc/skel/.llupgradebackup/7.0 2>&1 | tee -a $LOGFILE
  cp /etc/skel/.config/Mousepad/accels.scm /etc/skel/.llupgradebackup/7.0 2>&1 | tee -a $LOGFILE
}

# Backup /etc/skel/ Linux Lite 7.2

BUPETCSKEL7264() {
  log_upgrade "BUPETCSKEL7264"
  echo "#⚫ Backing up /etc/skel files..." && sleep 2
  mkdir -p /etc/skel/.llupgradebackup/7.2 2>&1 | tee -a $LOGFILE
  cp /etc/skel/.config/Mousepad/accels.scm /etc/skel/.llupgradebackup/7.2 2>&1 | tee -a $LOGFILE
}

# Get list of available updated packages
UPDPKGS() {
  log_upgrade "UPDPKGS"
  UPDATER=$( stdbuf -oL /bin/bash \-c '( apt-get update \-y )' 2>&1 | tee -a $LOGFILE | stdbuf -oL sed -n -e 's/^\(.\{128\}\).*/\1/' -e '/\[*$/ s/^/# /p' -e '/\*$/ s/^/# /p' |
  zenity --progress --width=600 --auto-close --no-cancel --percentage="$PERCENTAGE" --title="Updating repositories..." --text="Updating repositories...")
  if [ $? = 1 ]; then zenity --info --width="300" --title="$APPNAME" --text="\nError:\n${line}"; fi
unset UPDATER
}

# Erase existing available info
ERASEDPKGS() {
  log_upgrade "ERASEDPKGS"
  UPDATER=$( stdbuf -oL /bin/bash \-c '( dpkg --clear-avail )' 2>&1 | tee -a $LOGFILE | stdbuf -oL sed -n -e 's/^\(.\{128\}\).*/\1/' -e '/\[*$/ s/^/# /p' -e '/\*$/ s/^/# /p' |
  zenity --progress --width=600 --auto-close --no-cancel --percentage="$PERCENTAGE" --title="Clearing dpkg..." --text="Clearing dpkg...")
  if [ $? = 1 ]; then zenity --info --width="300" --title="$APPNAME" --text="\nError:\n${line}"; fi
unset UPDATER
}

# Install new packages, remove obsolete packages, clean up - 64BIT
INPKCL64() {
  log_upgrade "INPKCL64"
  UPDATER=$( stdbuf -oL /bin/bash \-c '(DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" \-y && apt-get install lite-thememanager sshpass \-y && apt-get autoremove \-y && apt-get clean )' 2>&1 | tee -a $LOGFILE |
  stdbuf -oL sed -n -e 's/^\(.\{128\}\).*/\1/' -e '/\[*$/ s/^/# /p' -e '/\*$/ s/^/# /p' |
  zenity --progress --width=600 --auto-close --no-cancel --percentage="$PERCENTAGE" --title="Downloading and Installing new packages..." --text="Downloading and Installing new packages...")
unset UPDATER
}

# Add new LibreOffice Version
UPGLOFFICE() {
  log_upgrade "UPGLOFFICE"
  UPDATER=$( stdbuf -oL /bin/bash \-c '(mkdir -p /tmp/lofficeupg && cd /tmp/lofficeupg && wget http://repo.linuxliteos.com/upgrade/7.4/libreoffice/loffice74.tar.gz &&  tar -xvf loffice74.tar.gz -C /tmp/lofficeupg/ && cd /tmp/lofficeupg/ && dpkg -i *.deb && sleep 2 && rm -rf /tmp/lofficeupg/ && rm -rf /usr/share/applications/libreoffice7.2* && rm -rf /usr/share/applications/libreoffice24* && rm -rf /usr/share/applications/libreoffice-startcenter.desktop && rm -rf /usr/share/applications/libreoffice7.3-draw.desktop && rm -rf /usr/share/applications/libreoffice7.3-math.desktop && rm -rf /usr/share/applications/libreoffice24.2-math.desktop && sleep )' 2>&1 | tee -a $LOGFILE |
  stdbuf -oL sed -n -e 's/^\(.\{128\}\).*/\1/' -e '/\[*$/ s/^/# /p' -e '/\*$/ s/^/# /p' |zenity --progress --width=600 --auto-close --no-cancel --percentage="$PERCENTAGE" --title="Downloading, Installing & Upgrading LibreOffice...")
unset UPDATER
}

# Remove outdated LibreOffice packages
OUTLOPKGS() {
  log_upgrade "OUTLOPKGS"
  UPDATER=$( stdbuf -oL /bin/bash \-c '( apt-get remove --purge libreoffice7.6* libobasis7.6* libreoffice7.4-ure  \-y )' 2>&1 | tee -a $LOGFILE | stdbuf -oL sed -n -e 's/^\(.\{128\}\).*/\1/' -e '/\[*$/ s/^/# /p' -e '/\*$/ s/^/# /p' |
  zenity --progress --width=600 --auto-close --no-cancel --percentage="$PERCENTAGE" --title="LibreOffice" --text="Removing outdated LibreOffice packages...")
  if [ $? = 1 ]; then zenity --info --width="300" --title="$APPNAME" --text="\nError:\n${line}"; fi
unset UPDATER
}

# Update lsb_release and issue files /etc changes, update /etc/llver
UPLSBISSUE() {
  log_upgrade "UPLSBISSUE"
  UPDATER=$( stdbuf -oL /bin/bash \-c '( sed -i "s/DISTRIB_DESCRIPTION=.*/DISTRIB_DESCRIPTION=\"Linux Lite "'$UPGR'"\"/g" /etc/lsb-release && echo "Linux Lite $UPGR LTS \n \l" | tee /etc/issue && echo "Linux Lite $UPGR" | tee /etc/llver && sleep 2 )' 2>&1 | tee -a $LOGFILE | stdbuf -oL sed -n -e '/\[*$/ s/^/# /p' -e '/\*$/ s/^/# /p' |
  zenity --progress --width=600 --auto-close --no-cancel --percentage="$PERCENTAGE" --title="Updating version information...")
  if [ $? = 1 ]; then zenity --info --width="300" --title="$APPNAME" --text="\nError:\n${line}"; fi
unset UPDATER
}

# Update grub
UPDGRUB() {
  log_upgrade "UPDGRUB"
  UPDATER=$( stdbuf -oL /bin/bash \-c '( update-grub && sleep 2 )' 2>&1 | tee -a $LOGFILE | stdbuf -oL sed -n -e '/\[*$/ s/^/# /p' -e '/\*$/ s/^/# /p' |
  zenity --progress --width=600 --auto-close --no-cancel --percentage="$PERCENTAGE" --title="Updating Grub..." --text="Updating Grub...")
  if [ $? = 1 ]; then zenity --info --width="300" --title="$APPNAME" --text="\nError:\n${line}"; fi
unset UPDATER
}

# Update /usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth
UPDPLYM() {
  log_upgrade "UPDPLYM"
  UPDATER=$( stdbuf -oL /bin/bash \-c '( sed -i "s/^title=Linux Lite.*$/title=Linux Lite "'$UPGR'"/g" /usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth ;
  sed -i "s/^title=Ubuntu.*$/title=Linux Lite "'$UPGR'"/g" /usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth ;
  sed -i "s/black=0x2c001e/black=0x000000/g" /usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth ;
  sed -i "s/white=0xffffff/white=0xffffff/g" /usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth ;
  sed -i "s/brown=0xff4012/brown=0xffff00/g" /usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth ;
  sed -i "s/blue=0x988592/blue=0x000000/g" /usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth && update-initramfs -u && sleep 2 )' 2>&1 | tee -a $LOGFILE |
  stdbuf -oL sed -n -e 's/^\(.\{128\}\).*/\1/' -e '/\[*$/ s/^/# /p' -e '/\*$/ s/^/# /p' |
  zenity --progress --width=600 --auto-close --no-cancel --percentage="$PERCENTAGE" --title="Updating boot configuration please wait...")
  if [ $? = 1 ]; then zenity --info --width="300" --title="$APPNAME" --text="\nError:\n${line}"; fi
unset UPDATER
return
}


#Check the version and start the appropriate function
LLVER=$(awk '{print $3}' /etc/llver)

  if [ "$LLVER" == "7.0" ]; then LL70SYSTEM
elif [ "$LLVER" == "7.2" ]; then LL72SYSTEM
elif [ "$LLVER" == "7.4" ]; then LL74SYSTEM
elif [ "$LLVER" == "7.6" ]; then LL76SYSTEM
elif [ "$LLVER" == "7.8" ]; then zenity --info --width="300" --title="$_APPNAME" --text="\nYou are already running the latest release."; exit 0
  fi

# Check if ARRAYB is empty
# If it is skip the execution
if [ ${#ARRAYB[@]} -ne 0 ]; then
    x=0
    for k in "${!ARRAYB[@]}"; do  
        x=$(( x + 1 ))  # Get the total number of selected items in array B
    done
    TOTAL_LINES=$x
    i=0
    printf '%s \n' "${ARRAYB[@]}" | while read -r line; do
        eval "$line"  # Execute functions one by one
        if [ $? -ne 0 ]; then
            log_upgrade "Error - $line"
            zenity --error --width="300" --title="$APPNAME" --text="An error occurred while performing upgrades:\n${line}"
            exit 1
        fi
        ((i++))
        (( PERCENTAGE = 100 * i / TOTAL_LINES ))
        echo "$PERCENTAGE"
        if [ "$PERCENTAGE" -eq 100 ]; then
            echo "#⚫ Completed." && sleep 2
        fi
    done | zenity --progress --title="$APPNAME" --auto-kill --auto-close --no-cancel --width=400
fi

# Check if ARRAYC is empty
# If it is skip the execution
if [ ${#ARRAYC[@]} -ne 0 ]; then
    x=0
    for k in "${!ARRAYC[@]}"; do  
        x=$(( x + 1 ))  
    done  # Get the total number of selected items in the array

    i=0
    TOTAL_LINES=$x
    PERCENTAGE=0

    printf '%s\n' "${ARRAYC[@]}" | while read -r line; do
        (( PERCENTAGE = 100 * i / TOTAL_LINES ))
        
        eval "$line"  # Execute functions one by one
        
        if [ $? -eq 1 ]; then 
            log_upgrade "Error - $line"
            zenity --error --width="300" --title="$APPNAME" --text="An error occurred while performing upgrades:\n ${line}"
        fi
        
        ((i++))

        if [ "$PERCENTAGE" -eq 100 ]; then 
            echo "#⚫ Completed." 
            sleep 2
        fi
    done
fi


unset ARRAYB
unset ARRAYC
unset MYUSER

