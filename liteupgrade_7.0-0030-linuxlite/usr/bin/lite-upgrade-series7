#!/bin/bash
#----------------------------------------------------------------
# Description: Linux Lite Upgrade Script - USER
# About: For Linux Lite 7.- Series only
# Licence: GPLv2 Sharing is Caring
# Author: Jerry Bezencon, Ralphy
# Credits: Misko_2083
# Website: https://www.linuxliteos.com
#----------------------------------------------------------------

# Ensure multi-language support
export LANG=C

# Set display output
xhost +
export DISPLAY=:0.0

# Release Upgrade
UPGR="7.4"
UPGRFULL="Linux Lite 7.4"

# Get current Linux Lite version
GETLLVER=$(awk '{print}' /etc/llver)

# Admin user variable
MYUSER=$(sudo -u ${SUDO_USER:-$USER} whoami)

# Linux Lite default dialog icon
ic="/usr/share/icons/Papirus/24x24/apps/liteupgrade.png"		# dialog icon 24x24 PNG

# Application title
APPNAME="Linux Lite Upgrade Utility"

# Log file variables
LOGFILE=/tmp/lite-upgrade.log
REPORTERROR="This error has been logged to $LOGFILE."
if [ -f "$LOGFILE" ]; then chown $MYUSER:$MYUSER $LOGFILE; fi
# log function
log_upgrade(){
    message="$@"
    echo '['$(date +%D\ %H:%M:%S)'] '$message >>$LOGFILE
}
> $LOGFILE
log_upgrade "---------- $APPNAME ----------"

# FIRST LOCAL MODIFICATION HERE

# Set lite-welcome to autostart on first boot - NOW DEFUNCT
LITEWELCOME() {
  echo " "
unset UPDATER
}

# END LOCAL MODIFICATION HERE

REBOOT() {
  log_upgrade "Waiting for restart confirmation..." && log_upgrade "Moving upgrade log to the /var/log/ directory... All done."
    mv -f "$LOGFILE" /var/log/
while (true); do
  # Set dialog icon variable
  THEME=$(xfconf-query -c xsettings -p /Net/IconThemeName)
  if [ -f "/home/$USER/.icons/${THEME}/apps/32/system-reboot.png" ]; then APPICON="/home/$USER/.icons/${THEME}"
  elif [ -f "/usr/share/icons/${THEME}/32x32/apps/system-reboot.svg" ]; then APPICON="/usr/share/icons/${THEME}"
  else APPICON="/usr/share/icons/Papirus"
  fi

RESTA=/usr/share/icons/Papirus/32x32/apps/system-reboot.svg
SHUTD=/usr/share/icons/Papirus/32x32/apps/system-shutdown.svg
VLG=/usr/share/icons/Papirus/32x32/apps/accessories-text-editor.svg

  yad --window-icon="$ic" --no-buttons --buttons-layout="center" --text-align="center" --borders="20" --title "           Lite Upgrade" --text='<span font="Sans Bold 13">Upgrade routine completed</span>\n----------------------------------------------------------------\nSave and Close any applications\nyou may have open\nbefore choosing an option.\n' --form --width=350 \
--field="Restart!$RESTA":fbtn "systemctl reboot" \
--field="Shutdown!$SHUTD":fbtn "systemctl poweroff" \
--field="View Log!$VLG":fbtn "xdg-open /var/log/lite-upgrade.log" \
--button=gtk-close:1

  done
}

LL70() {
# Invoke arrays
ARRAYA=()       # Array for LL 7.0

# check the arch
log_upgrade "Checking Architecture:"
arch=$(uname -m) ; log_upgrade "$arch"

case "$arch" in
    x86)    arch="x86"  ;;
    i?86)   arch="x86"  ;;
    amd64)  arch="amd64"    ;;
    x86_64) arch="amd64"    ;;
    * ) echo "Your Arch '$arch' -> Its not supported."  ;;
esac

# Populate arrays based on the arch
case "$arch" in
    x86)
    ARRAYA+=('LITEWELCOME')
    ;;
    amd64)
    ARRAYA+=('LITEWELCOME')
    ;;
esac
}

LL72() {
# Invoke arrays
ARRAYA=()       # Array for LL 7.2

# check the arch
log_upgrade "Checking Architecture:"
arch=$(uname -m) ; log_upgrade "$arch"

case "$arch" in
    x86)    arch="x86"  ;;
    i?86)   arch="x86"  ;;
    amd64)  arch="amd64"    ;;
    x86_64) arch="amd64"    ;;
    * ) echo "Your Arch '$arch' -> Its not supported."  ;;
esac

# Populate arrays based on the arch
case "$arch" in
    x86)
    ARRAYA+=('LITEWELCOME')
    ;;
    amd64)
    ARRAYA+=('LITEWELCOME')
    ;;
esac
}

RUN() {
  log_upgrade "Executing upgrades..."
# Check if ARRAYA is empty - If it is empty skip the execution
if [ ${#ARRAYA[@]} -ne 0 ]; then
    TOTAL_LINES=${#ARRAYA[@]}  # Get the total number of selected items in array A
    i=0  # Initialize counter
    printf '%s\n' "${ARRAYA[@]}" | while read -r line; do
      eval "$line"  # Execute functions one by one safely
      exit_code=$?  # Capture exit status

      if [ "$exit_code" -eq 1 ]; then 
        log_upgrade "Error - $line"
        zenity --error --width="300" --title="$APPNAME" --text="\nAn error occurred while performing upgrades:\n ${line}"
        exit 1
      fi

      ((i++))  # Increment counter
      PERCENTAGE=$(( 100 * i / TOTAL_LINES ))
      echo "$PERCENTAGE"

      if [ "$PERCENTAGE" -eq 100 ]; then 
        echo "#⚫ Completed."
        sleep 2
      fi
    done | zenity --progress --title="$APPNAME" --auto-kill --auto-close --no-cancel --width=400
fi


export -f log_upgrade; export MYUSER REPORTERROR UPGR APPNAME LOGFILE IC
/usr/bin/lite-upgrade-system-series7     # Run the next script

if grep "Linux Lite ${UPGR}" /etc/llver; then log_upgrade "Upgrade succeeded."; REBOOT; else log_upgrade "Upgrade failed - /etc/llver is not ${UPGR}"; zenity --info --title="$APPNAME" --text="Upgrade canceled."; fi
}

upgrade_exec() {
# check LL current version and start the appropriate function
log_upgrade "Upgrading version:"
# check if version release exists in /etc/llver. If not, create it from /etc/issue file
if [ "$(grep -c "Linux Lite" /etc/llver)" -ge 1 ]; then :; else grep "Linux Lite" /etc/issue | cut -c 1-14 > /etc/llver; fi
# check LL version and start the appropriate function
if grep "Linux Lite 7.0" /etc/llver; then log_upgrade "Linux Lite 7.0"; LL70
elif grep "Linux Lite 7.2" /etc/llver; then log_upgrade "Linux Lite 7.2"; LL72
#elif grep "Linux Lite 7.4" /etc/llver; then log_upgrade "Linux Lite 7.4"; LL74
#elif grep "Linux Lite 7.6" /etc/llver; then log_upgrade "Linux Lite 7.6"; LL76
#elif grep "Linux Lite 7.8" /etc/llver; then log_upgrade "Linux Lite 7.8"; LL78
#else latest_version
else
	echo "nothing"
#	zenity --info --title="$APPNAME" --text="msg here"
fi
}

# We start here
latest_version() { if grep "Linux Lite 7.4" /etc/llver; then log_upgrade "Linux Lite $UPGR"; zenity --info --title="No need to Upgrade" --window-icon="$ic" --width="300" --text="\nYou are already running the latest release."; exit 0 ; else :; fi }
latest_version

# First dialog box - we must ask for elevation from the start otherwise a regular user can run lite-upgrade-series7 unnecessarily
if [ $EUID -ne 0 ]; then
  zenity --question --icon-name="info" --width="430" --title="$APPNAME" \
         --window-icon="$ic" --cancel-label="Cancel" --ok-label="Upgrade" \
         --text="\nYou are currently running: <b>$GETLLVER</b>\n
You are about to upgrade to: <b>$UPGRFULL</b>\n
Your computer must remain connected to the internet during this time.\n
A connectivity check to the Linux Lite repositories will be conducted\n
to make sure that the connection with the server can be established.\n
Once upgrades have taken place, you must restart the system to complete\n
the upgrade process.\n\n
Please save and close your work <b>now</b> before continuing.\n\n
Click <b>Cancel</b> to exit now or <b>Upgrade</b> to proceed.\n"
# If No is clicked then exit
if [ "${PIPESTATUS[0]}" -ne "0" ]; then
log_upgrade "user canceled - main dialog"
exit 0
fi

case $? in
    0) log_upgrade "Checking Internet access..."
    if eval "curl -sk google.com" >> /dev/null 2>&1; then
      :; log_upgrade "Internet connection check passed!"
    else # Prompt ERROR internet connection check failed and exit
        zenity --info --width=320 --height=120 --ok-label="Close" --title="$APPNAME - Aborted" \
                --text="\n<b>Your computer is not connected to the internet</b> \n\n$APPNAME cannot continue. \nPlease check your Internet connection and try again."
            log_upgrade "the computer is not connected to the internet - $APPNAME aborted."
            exit 0
    fi
    log_upgrade "Checking connectivity to Master Repository..."
    curl -sk repo.linuxliteos.com/linuxlite/db/version
        if [ ${?} -ne 0 ]; then
          log_upgrade "Cannot connect to Linux Lite Master Repository - aborted"
          zenity --error --title="$APPNAME - Aborted" --width="320" --ok-label="Close" \
          --text="\nLinux Lite Upgrade Utility was unable to establish connection with the Linux Lite master repository.\n
Please check your internet connection and try again.\n\nUpgrade has been canceled."
          exit 0
        else
          log_upgrade "Linux Lite Master is ONLINE - passed."; :
        fi

        pkexec $0; if [ "${PIPESTATUS[@]}" -eq "126" ] || [ "${PIPESTATUS[@]}" -eq "127" ] ; then log_upgrade "Authentication canceled - aborted"; exit 0 ; fi ;;
    1) :;;
    esac
fi

test -e ["/var/log/lite-upgrade.log"] || rm -rf /var/log/lite-upgrade.log
test -e ["/tmp/lite-upgrade.log"] || rm -rf /tmp/lite-upgrade.log

upgrade_exec
RUN

exit 0
